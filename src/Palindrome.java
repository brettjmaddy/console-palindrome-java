/*
    Brett Maddy
    CS 3230 ONL Fall 17 23451
    Lab 1
*/
import java.util.Scanner;

public class Palindrome {

    
    public static void main(String[] args) 
    {
        Scanner input;
        String potentialPalin = "";
        
        //If the user did not enter any input, then they will be prompted to do so.
        //Their input will then be stripped of spaces and used to create a new Scanner object
        if(args.length == 0)
        {
            System.err.println("-Please enter a potential palindrome-");
            input = new Scanner(System.in);
            potentialPalin = input.nextLine();
            potentialPalin = potentialPalin.replace(" ", "");            
        }
        //If the user did enter input right away, the elements of the args array
        //will be concatenated onto the potential palindrome string
        else
        {
            for(String s : args)
            {
                potentialPalin += s;
            }            
        }
        
        //Using the StringBuilder method reverse, we reverse the order of the characters
        //of the potential palindrome string
        String reversed = new StringBuilder(potentialPalin).reverse().toString();
        
        //Are the reversed string and orginal string equal?
        //If so, the user is congratulated. If not, the user is told to try again.
        if(potentialPalin.equals(reversed))
        {
            System.out.println("\nGreat! You have entered a proper palindrome!");
            //System.out.println(potentialPalin);
            //System.out.println(reversed);
        }
        else
        {
            System.err.println("\nSorry, that is not a proper palindrome. You should try again.");
            
        }        
        
    }
    
}
